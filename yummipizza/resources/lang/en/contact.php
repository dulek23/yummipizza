<?php

return [
    'contact_us' => 'Contact us',
    'full_name' => 'Full name',
    'email' => 'E-mail',
    'message' => 'Message',
    'send' => 'Send',
    'phone' => 'Phone',
    'success'=>'Your message was sent!'
];
