<?php

return[
    'breakfast' => 'Breakfast',
    'pizza' => 'Pizza',
    'pasta' => 'Pasta',
    'pirogue' => 'Pirogue',
    'lasagne' => 'Lasagne',
    'meat' => 'Meat',
    'grill' => 'Grill',
    'salad' => 'Salad',
    'fish' => 'Fish',
    'olives' => 'Olives',
    'dessert' => 'Dessert',
    'drinks' => 'Drinks',
    'sandwiches' => 'Sandwiches',

    'order' => 'Order',
    'price' => 'Price',


];
