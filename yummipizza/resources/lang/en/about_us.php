<?php

return [
    'our_history' => 'Our history',
    'created_by' => 'Yummy pizzeria was created out of owner, who wants to apply his knowledge and many years of experience from this business to fulfil different types of customer wishes.',
    'about_pizza' => 'In our pizzeria, the most important are quality food and fast delivery. Our pizzas are prepared with specially selected flour and dough that stands for at least 72 hours to achieve the best probability.',
    'enjoy' => 'Enjoy our other specialties such as delicious risotto, salads for all tastes, various cooked dishes…',
];
