<?php

    return[
        'error' => 'The article is in a cart!',
        'cart' => 'Cart',
        'min_order' => 'Minimum order:',
        'your_order' => 'Your order',
        'edit' => 'Edit',
        'price' => 'Price:',
        'continue_shopping' => 'Continue shopping',
        'success' => 'You have successfully placed your order!',

        //order checkout
        'checkout' => 'Checkout',
        'delivery_info' => 'Delivery information',
        'name' => 'Name',
        'phone' => 'Phone',
        'address' => 'Address',
        'order' => 'Order',
        'order_msg' => 'Are you confirm order?',
        'close' => 'Close',
        'confirm' => 'Confirm',

        'order_change' => 'Order change',
    ];

?>
