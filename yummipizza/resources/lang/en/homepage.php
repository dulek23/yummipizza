<?php

return[
    'fresh' => 'FRESH AND TASTY',
    'delivery' => 'FAST DELIVERY',
    'experience' => '20 YEARS OF EXPERIENCE',
    'production' => 'All products are stored at the time of receipt of the order.',
    'respond' => 'We respond quickly to your call or mail and bring you your favorite specialties.',
    'confirmation' => 'Confirmation of our quality are 20 years of work and experience.',

    'pizza' => 'Pizza',
    'recognizable' => 'Yummi pizzeria is recognizable by its long tradition in preparing pizzas.',
    'pizza_prepare' => 'All pizzas are prepared from fresh and homemade ingredients and pizzas are prepared at the time of receipt of your order.',
    'pizza_delivery' => 'Pizza delivery is done in the period from 10 to 23.',

    'grill' => 'Grill',
    'grill_lovers' => 'Yummy pizzeria is the right place for big grill lovers.',
    'grill_specialities' => 'Grilled specialities are prepared by top caterers who make the juiciest grill meals for you.',
    'grill_prepare' => 'The grill is prepared from fresh and home-made meat. All grilled specialities are prepared on time so that they are delivered hot.',

    'sandwiches' => 'Sandwiches',
    'sandwiches_prepare' => 'Yummi pizzeria prepares and delivers sandwiches to the desired address.',
    'hot_sandwiches' => 'Sandwiches are the ideal solution for a tasty and quick meal. Hot sandwiches are an ideal substitute for lunch if you are at work.',
    'love_sandwiches' => 'We all love sandwiches, and even if they are warm and your favorite cheese is poured over them, we would hardly want anything more.'
];
