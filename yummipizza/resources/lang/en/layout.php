<?php

return [

    //menu
    'home' => 'Homepage',
    'delivery_menu' => 'Menu',
    'restaurant_menu' => 'Restaurant menu',
    'about_us' => 'About us',
    'contact' => 'Contact',
    'pizza' => 'Pizza',

    //footer
    'menu' => 'Menu',
    'mobile' => 'Mobile',
    'phone' => 'Phone',
    'work_time' => 'Work time',
    'kitchen' => 'Kitchen',
    'mon_fri' => 'Mon - Fri',
    'sat_sun' => 'Sat - Sun',
    'restaurant' => 'Restaurant',
];
