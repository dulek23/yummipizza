<?php

return[
    'breakfast' => 'Doručak',
    'pizza' => 'Pica',
    'pasta' => 'Pasta',
    'pirogue' => 'Piroške',
    'lasagne' => 'Lazanje',
    'meat' => 'Meso',
    'grill' => 'Roštilj',
    'salad' => 'Salate',
    'fish' => 'Riba',
    'olives' => 'Masline',
    'dessert' => 'Poslastice',
    'drinks' => 'Piće',
    'sandwiches' => 'Sendviči',

    'order' => 'Naruči',
    'price'=> 'Cena',

    'error' => 'Artikal se već nalazi u korpi!'
];
