<?php

return [

    //menu
    'home' => 'Naslovna',
    'delivery_menu' => 'Meni',
    'restaurant_menu' => 'Meni restorana',
    'about_us' => 'O nama',
    'contact' => 'Kontakt',
    'pizza' => 'Pica',

    //footer
    'menu' => 'Meni',
    'mobile' => 'Mob',
    'phone' => 'Tel',
    'work_time' => 'Radno vreme',
    'kitchen' => 'Kuhinja',
    'mon_fri' => 'Pon - Pet',
    'sat_sun' => 'Sub - Ned',
    'restaurant' => 'Restoran',
];
