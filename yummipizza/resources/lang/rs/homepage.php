<?php

return[
    'fresh' => 'SVEŽE I UKUSNO',
    'delivery' => 'BRZA DOSTAVA',
    'experience' => '20 GODINA ISKUSTVA',
    'production' => 'Svi proizvodi se spremaju u trenutku prijema porudžbine.',
    'respond' => 'Reagujemo munjevito na Vaš poziv ili mail i dovozimo Vam omiljene specijalitete.',
    'confirmation' => 'Potvrda našeg kvaliteta su 20 godina rada i iskustva.',

    'pizza' => 'Pica',
    'recognizable' => 'Yummi pizzeria je prepoznatljiva po dugogodišnjoj tradiciji u pripremanju pica.',
    'pizza_prepare' => 'Sve pice se pripremaju od svežih i domaćih namirnica i pice se pripremaju u trenutku prijema vaše porudžbine.',
    'pizza_delivery' => 'Dostava pica se vrši u periodu od 10-23h',

    'grill' => 'Rostilj',
    'grill_lovers' => 'Yummy pizzeria je pravo mesto za velike ljubitelje roštilja.',
    'grill_specialities' => 'Specijalitete sa roštilja pripremaju vrhunski ugostitelji koji za vas prave najsočnije obroke od roštilja.',
    'grill_prepare' => 'Roštilj se priprema od svežeg i domaćeg mesa. Svi specijaliteti sa roštilja se pripremaju za zakazano vreme tako da se isporučuju topla.',

    'sandwiches' => 'Sendviči',
    'sandwiches_prepare' => 'Yummi pizzeria priprema i vrši isporuku sendviča na željenu adresu.',
    'hot_sandwiches' => 'Sendviči su idealno rešenje za ukusan i brz obrok. Topli sendviči su idealna zamena za ručak ukoliko ste na poslu.',
    'love_sandwiches' => 'Svi volimo sendviče, a još ako su topli i ako se preko njih preliva omiljeni sir, teško da bismo poželeli nešto više.'
];
