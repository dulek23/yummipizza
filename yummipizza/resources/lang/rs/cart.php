<?php

return[
    'error' => 'Artikal se već nalazi u korpi!',
    'cart' => 'Korpa',
    'min_order' => 'Minimalna porudžbina:',
    'your_order' => 'Vaša nadrudžbina',
    'edit' => 'Izmeni',
    'price' => 'Cena:',
    'continue_shopping' => 'Nastavi kupovinu',
    'success' => 'Uspešno ste izvršili kupovinu!',

    //placanje
    'checkout' => 'Plaćanje',
    'delivery_info' => 'Podaci za isporuku',
    'name' => 'Ime',
    'phone' => 'Telefon',
    'address' => 'Adresa',
    'order' => 'Naruči',
    'order_msg' => 'Da li ste sigurni da želite da potvrdite kupovinu?',
    'close' => 'Otkaži',
    'confirm' => 'Potvrdi',

    'order_change' => 'Izmeni narudžbinu',
];

?>
