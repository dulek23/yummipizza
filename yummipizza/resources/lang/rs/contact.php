<?php

return [
    'contact_us' => 'Kontaktirajte nas',
    'full_name' => 'Ime i prezime',
    'email' => 'E-mail',
    'message' => 'Poruka',
    'send' => 'Pošalji',
    'phone' => 'Telefon',
    'success'=>'Vaša poruka je uspešno poslata!'
];
