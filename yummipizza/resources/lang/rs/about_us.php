<?php

return [
    'our_history' => 'Naša istorija',
    'created_by' => 'Yummy pizzeria je stvorena od strane vlasnika, koji želi da svoje znanje i dugogodišnje iskustvo primeni u ispunjavanju različitih želja mušterija.',
    'about_pizza' => 'U našoj piceriji, najvažniji su kvalitetna hrana i brza dostava. Naše pice se pripremaju od posebno odabranog brašna i testa koje odstoji najmanje 72 sata kako bi se postigla najbolja verovatnoća.',
    'enjoy' => 'Uživajte u našim drugim specijalitetima poput ukusnog rižota, salate za svačiji ukus, raznim kuvanim jelima...',
];
