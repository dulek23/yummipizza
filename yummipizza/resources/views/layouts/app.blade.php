<?php
    use \Illuminate\Support\Facades\App;
    use App\Cart;
?>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Yummi pizza</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/9f801b9860.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Philosopher:wght@700&display=swap" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mystyle.css') }}" rel="stylesheet">


</head>
<body>

<div id="app" class="content_bg_color">
    <nav class="navbar navbar-expand-lg navbar-light shadow-sm d-flex justify-content-center" id="header_background">
        <div style="width: 900px;">
            <a class="navbar-brand d-lg-none" href="{{ url('/') }}">
                <span id="header_title">The Yummi pizza</span>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a href="/cart" class="d-lg-none pl-3">
                <img src="/img/shopping-cart.png">
                <span id="articles_in_cart">({{ Cart::count() }})</span>
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav m-auto align-items-center pl-5">

                    <li class="nav-item link_space">
                        <a class="nav-link header_links" href="/">{{ __('layout.home') }}</a>
                    </li>

                    <li class="nav-item link_space">
                        <a class="nav-link header_links" href="/menu">{{ __('layout.delivery_menu') }}</a>
                    </li>


                    <li class="d-none d-lg-block link_space">
                        <a href="/" style="text-decoration: none;">
                            <span id="header_title">Yummi pizza</span>
                        </a>
                    </li>

                    <li class="nav-item link_space">
                        <a class="nav-link header_links" href="/about-us">{{ __('layout.about_us') }}</a>
                    </li>

                    <li class="nav-item link_space">
                        <a class="nav-link header_links" href="/contact">{{ __('layout.contact') }}</a>
                    </li>

                </ul>
                <div class="my-auto d-flex justify-content-center align-items-center pl-4">
                    <div>
                        <select class="form-control" onChange="window.document.location.href=this.value;" style="width: 100px;">
                            <option value="/locale/rs" <?php if(App::getLocale()== "rs") echo "selected"?>>Serbian</option>
                            <option value="/locale/en" <?php if(App::getLocale()== "en") echo "selected"?>>English</option>
                        </select>
                    </div>

                    <div class="d-none d-lg-block ml-5">
                        <a href="/cart" style="text-decoration: none;">
                            <img src="/img/shopping-cart.png">
                            <span id="articles_in_cart">({{ Cart::count() }})</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>

<div class="container-fluid">
    <div class="row" id="footer_box">

        <div class="col-md-4 col-sm-12 d-flex justify-content-center footer_top_space">
            <div>
                <div>
                    <span class="footer_title">{{ __('layout.menu') }}</span>
                </div>

                <div class="footer_y_space">
                    <a href="/">
                        <span class="footer_links">{{ __('layout.home') }}</span>
                    </a>
                </div>
                <!--
                <div class="footer_y_space">
                    <a href="#">
                        <span class="footer_links">{{ __('layout.restaurant_menu') }}</span>
                    </a>
                </div>
                -->
                <div class="footer_y_space">
                    <a href="/contact">
                        <span class="footer_links">{{ __('layout.contact') }}</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12 d-flex justify-content-center footer_top_space">
            <div style="padding-left: 40px;">
                <div>
                    <span class="footer_title">{{ __('layout.contact') }}</span>
                </div>

                <div class="footer_y_space">
                    <span class="footer_links">{{ __('layout.mobile') }}: 064 535 23 24</span>
                </div>

                <div class="footer_borderline footer_y_space">
                    <span class="footer_links">{{ __('layout.phone') }}: 011 445 3145</span>
                </div>

                <div class="footer_borderline footer_y_space">
                    <div>
                        <span class="footer_links">Kneza Milosa 23</span>
                    </div>

                    <div class="footer_y_space">
                        <span class="footer_links">Beograd</span>
                    </div>
                </div>

                <div class="footer_y_space">
                    <span class="footer_links">yummipizza@gmail.com</span>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12 d-flex justify-content-center footer_top_space">
            <div style="padding-left: 73px;">
                <div>
                    <span class="footer_title">{{ __('layout.work_time') }}</span>
                </div>

                <div class="footer_y_space">
                    <span class="footer_links">{{ __('layout.kitchen') }}</span>
                </div>

                <div>
                    <span class="footer_links">{{ __('layout.mon_fri') }}</span>
                    <span class="footer_links pl-5">09:00-23:00</span>
                </div>

                <div class="footer_borderline pb-2">
                    <span class="footer_links">{{ __('layout.sat_sun') }}</span>
                    <span class="footer_links pl-5">09:00-23:00</span>
                </div>

                <div class="footer_y_space">
                    <span class="footer_links">{{ __('layout.restaurant') }}</span>
                </div>

                <div>
                    <span class="footer_links">{{ __('layout.mon_fri') }}</span>
                    <span class="footer_links pl-5">09:00-23:00</span>
                </div>

                <div class="pb-2">
                    <span class="footer_links">{{ __('layout.sat_sun') }}</span>
                    <span class="footer_links pl-5">09:00-23:00</span>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
