@extends('layouts.app')

@section('content')
    <div class="container" id="article_show_box">
        @if(Session::has('success'))
            <div class="alert alert-success d-flex justify-content-center" role="alert">
                <span class="font-weight-bold" style="font-size: 18px;">
                    {{ Session::get('success') }}
                </span>
            </div>
        @endif
        <div class="row align-items-center pb-5">
            <div class="col-4">
                <img src="/img/cart/left.png" class="w-100">
            </div>

            <div class="col-4 d-flex justify-content-center my-auto">
                <h1 style="color: #77411d; ">{{ __('cart.cart') }}</h1>
            </div>

            <div class="col-4">
                <img src="/img/cart/right.png" class="w-100">
            </div>
        </div>

        @if($cart->sum('sum')<600)
        <div class="row pt-5">
            <div class="col-12 d-flex justify-content-center">
                <h2 class="text-danger">{{ __('cart.min_order') }} 600 din</h2>
            </div>
        </div>
        @endif
        <div class="row pt-1 pb-3">
            <div class="col-12 d-flex justify-content-center">
                <h3>{{ __('cart.your_order') }}</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive-md">
                    <table class="table borderline_bottom">
                        @foreach($cart as $c)
                        <tr>
                            <td class="table_column align-middle table_text_data" id="borderline_top" style=" min-width: 120px;">{{ $c->translateCarts('article_name') }}</td>
                            <td class="table_column align-middle table_text_data" id="borderline_top">{{ $c->translateCarts('portion') }}</td>
                            <td class="d-flex justify-content-center" id="borderline_top">
                                <form action="/cart/{{ $c->id }}" method="POST">
                                   @method('PATCH')
                                    @csrf
                                    <div class="d-flex">
                                        <input type="number" name="quantity" class="form-control mr-2" value="{{ $c->quantity }}" min="1" style="width: 90px;">
                                        <button class="btn btn-success">{{ __('cart.edit') }}</button>
                                    </div>
                                </form>
                            </td>
                            <td class="table_column align-middle table_text_data" style=" min-width: 90px;" id="borderline_top">{{ $c->sum }} din</td>

                            <td id="borderline_top">
                                <a href="/cart/{{ $c->id }}" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                                    <img src="/img/cart/delete.png">
                                </a>

                                <form id="delete-form" action="/cart/{{ $c->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-10 offset-1 d-flex justify-content-end">
                <div class="d-flex">
                    @if($cart->count()>0)
                        <span class="price_text">{{ __('cart.price') }}</span>
                        <span class="price_text pl-3"> {{ $cart->sum('sum') }} din</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row pt-2">
            <div class="col-6 offset-3">
                <div class="d-flex justify-content-center">
                    <a class="btn btn-primary mr-3" href="/menu" style="min-width: 150px;">{{ __('cart.continue_shopping') }}</a>
                    <button class="btn btn-primary" onclick="location.href='/checkout'" <?php if($cart->sum('sum')<600){ ?> disabled <?php } ?> >{{ __('cart.checkout') }}</button>
                </div>
            </div>
        </div>

    <div>
@endsection


