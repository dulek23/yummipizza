@component('mail::message')

    {{ $data['name'] }}

    {{ $data['email'] }}

    {{ $data['message'] }}


@component('mail::button', ['url' => 'mailto:'.$data['email']])
Replay to {{ $data['name'] }}
@endcomponent


@endcomponent
