@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="min-height: 600px;">
        @if(\Illuminate\Support\Facades\Session::has('success'))
            <div class="alert alert-success d-flex justify-content-center font-weight-bold" role="alert">
                {{ \Illuminate\Support\Facades\Session::get('success') }}
            </div>
        @endif

        <div class=" row">
            <div class="col-md-12">
                <div class="d-flex justify-content-center" id="contact_top_box">
                    <span id="contact_title">{{ __('contact.contact_us') }}</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" id="contact_box_left">
                <div class="d-flex justify-content-center">
                    <form action="{{ route('contact.store') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-5 col-form-label">{{ __('contact.full_name') }}</label>

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="off">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">{{ __('contact.email') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="off">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="message" class="col-md-4 col-form-label">{{ __('contact.message') }}</label>

                            <div class="col-md-12">
                                <textarea id="message" class="form-control @error('message') is-invalid @enderror" name="message" value="{{ old('message') }}" autocomplete="off"></textarea>

                                @error('message')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-center">
                                <button class="btn btn-primary">{{ __('contact.send') }}</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

            <div class="col-md-6 d-flex justify-content-center">
                <div id="contact_box_right">
                    <div>
                        <span class="contact_right_title">{{ __('contact.email') }}</span>
                    </div>

                    <div class="contact_border_bottom">
                        <span class="contact_right_text">yummipizza@gmail.com</span>
                    </div>

                    <div>
                        <span class="contact_right_title">{{ __('contact.phone') }}</span>
                    </div>

                    <div>
                        <span class="contact_right_text">064 535 23 24</span>
                    </div>

                    <div class="contact_border_bottom">
                        <span class="contact_right_text">011 445 31 45</span>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
