<?php
use Illuminate\Support\Facades\App;
?>
@extends('layouts.app')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

@section('content')
    <div class="container p-0">
        <div class="table-responsive sticky-top" id="bg_color_gray">
            <table class="table m-auto" id="menu_width">
                <tr>
                    @foreach($cat as $c)
                        <td class="font_size_medium"><a href="/menu/#{{ strtolower($c->title_en) }}" class="font_green">{{ $c->translateCategory('title') }}</a></td>
                    @endforeach
                </tr>
            </table>
        </div>

        @foreach($cat as $c)
        <div class="padding_medium" id="{{ strtolower($c->title_en) }}">
            <span class="font-title" >{{ ucfirst($c->translateCategory('title')) }}</span>
        </div>

        <div class="row padding_left_medium">
            @foreach($c->articles as $article)
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex justify-content-center padding_top_medium pl-3 pr-3">
                <div class="card w-100">
                    <img class="img-fluid w-75 m-auto" src="/img/{{ $article->image }}" alt="Image">
                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                            <h5 class="card-title font-weight-bold">{{ $article->translate("name") }}</h5>
                        </div>
                        <div class="pb-2 d-flex justify-content-center">
                            <p class="card-text">{{ $article->translate("ingredients") }}</p>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center align-items-center align-baseline pb-3">
                        <div>
                            @if(count($article->article_price($article->id))>=2)
                                @foreach($article->article_price($article->id) as $a)
                                    <div>
                                        <span class="pr-3 font-weight-bold">
                                            <?php if(App::getLocale()=='en'){
                                                echo $a->title_en;
                                            }else{
                                                echo $a->title_rs;
                                                }
                                            ?>

                                            {{ $a->price }}
                                        </span>
                                    </div>
                                @endforeach
                            @else
                                @foreach($article->article_price($article->id) as $a)
                                    <div>
                                        <span class="pr-3 font-weight-bold"> {{ __('menu.price') }} {{ $a->price }}</span>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <a href="/menu/{{ $c->slug }}/{{ $article->slug }}" class="btn btn-primary">{{ __('menu.order') }}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach

        <a id="button"></a>
    </div>
@endsection

<script>

    jQuery(document).ready(function() {

        var btn = $('#button');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.show();
            } else {
                btn.hide();
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '400');
        });

    });

</script>
