@extends('layouts.app')

@section('content')
    <div class="container" id="article_show_box">
        <div class="row mt-5">
            <div class="col-md-6 col-sm-12 col-12 d-flex justify-content-center">
                <div>
                    <div class="d-flex justify-content-center">
                        <h3>{{ $article->translate('name') }}</h3>
                    </div>

                    <div>
                        <img src="/img/{{ $article->image }}" class="img-fluid">
                    </div>

                    <div class="pt-3 pb-5 m-auto" id="article_ingredients">
                        <span>{{ $article->translate('ingredients') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-12 d-flex justify-content-center my-auto">
                <div>
                    <form action="/cart" method="post">
                    @csrf
                        <input type="hidden" name="article_id" value="{{ $article->id }}">
                        <div id="portion_box">
                            <select id="price" name="price" class="form-control" onChange="myfunction()">
                                @foreach($article->portions as $ar_portion)
                                    <option value="{{ $ar_portion->price }}" >{{ $ar_portion->translatePortions('title') }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="portion_box">
                            <input class="form-control" type="number" value="1" min="1" name="quantity">
                        </div>

                        @if (Session::has('msg'))
                            <div class="pb-3">
                                <span class="text-danger">
                                    <strong>{{ Session::get('msg') }}</strong>
                                </span>
                            </div>
                        @endif

                        <div>
                            <button class="btn btn-primary">Dodaj u korpu</button>
                        </div>
                    </form>


                    <div>
                        <span style="font-size: 19px; font-family: serif; font-weight: bold;">{{ __('menu.price') }}: </span>
                        <span style="font-size: 18px; font-weight: bold; color: green;" id="portion_price">
                            {{ $portion_price->price }} din
                        </span>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

<script>
    function myfunction(){
        var x = document.getElementById("price").value;
        document.getElementById("portion_price").innerHTML = x + " din";
    }
</script>
