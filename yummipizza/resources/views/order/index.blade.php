@extends('layouts.app')

@section('content')
    <div class="container" id="article_show_box">
        <div class="row align-items-center pt-5 pb-5">
            <div class="col-4">
                <img src="/img/cart/left.png" class="w-100">
            </div>

            <div class="col-4 d-flex justify-content-center my-auto">
                <h1 style="color: #77411d; ">{{ __('cart.checkout') }}</h1>
            </div>

            <div class="col-4">
                <img src="/img/cart/right.png" class="w-100">
            </div>
        </div>

        <div class="row pb-4">
            <div class="col-md-12">
                <div class="d-flex justify-content-center">
                    <h2>{{ __('cart.delivery_info') }}</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <form action="/checkout" method="post" id="checkout">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right d-flex justify-content-center">{{ __('cart.name') }}</label>

                        <div class="col-md-12">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="off">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right d-flex justify-content-center">{{ __('cart.phone') }}</label>

                        <div class="col-md-12">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autocomplete="off">

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right d-flex justify-content-center">{{ __('cart.address') }}</label>

                        <div class="col-md-12">
                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" autocomplete="off">

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>

                   <div class="d-flex justify-content-center pt-4 pb-4">
                       <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                           {{ __('cart.order') }}
                       </button>
                   </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('cart.order') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <span style="font-size: 17px;">
                                        {{ __('cart.order_msg') }}
                                    </span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('cart.close') }}</button>
                                    <button type="submit" class="btn btn-primary">{{ __('cart.confirm') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row pt-5 pb-3">
            <div class="col-12 d-flex justify-content-center">
                <h3>{{ __('cart.your_order') }}</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive-md">
                    <table class="table borderline_bottom">
                        @foreach($cart as $c)
                            <tr>
                                <td class="table_column align-middle table_text_data" id="borderline_top" style=" min-width: 120px;">{{ $c->translateCarts('article_name') }}</td>
                                <td class="table_column align-middle table_text_data" id="borderline_top">{{ $c->translateCarts('portion') }}</td>
                                <td class="table_column align-middle table_text_data" id="borderline_top">
                                    {{ $c->quantity }}
                                </td>
                                <td class="table_column align-middle table_text_data" style=" min-width: 90px;" id="borderline_top">{{ $c->sum }} din</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="row pb-5">
            <div class="col-10 offset-1 d-flex justify-content-end">
                <div class="d-flex">
                    @if($cart->count()>0)
                        <span class="price_text">{{ __('cart.price') }}</span>
                        <span class="price_text pl-3"> {{ $cart->sum('sum') }} din</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row pb-5">
            <div class="col-6 offset-3 d-flex justify-content-center">
                <div class="mr-3">
                    <a href="/menu" class="btn btn-secondary">{{ __('cart.continue_shopping') }}</a>
                </div>

                <div>
                    <a href="/cart" class="btn btn-secondary">{{ __('cart.order_change') }}</a>
                </div>
            </div>
        </div>
    <div>
@endsection


