@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="min-height: 600px;">
        <div class="row" id="about_page_box">
            <div class="col-md-6">
                <div class="d-flex justify-content-center">
                    <div>
                        <img src="/img/about-pizza.jpg" alt="image" class="w-100">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div>
                    <div>
                        <span id="about_page_title">{{ __('about_us.our_history') }}</span>
                    </div>

                    <div>
                        <span>{{ __('about_us.created_by') }}</span>
                    </div>

                    <div class="pt-2">
                        <span>{{ __('about_us.about_pizza') }}</span>
                    </div>

                    <div class="pt-2">
                        <span>{{ __('about_us.enjoy') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
