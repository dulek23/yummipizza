@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 p-0">
                <div>
                    <img src="/img/home/cover.jpg" alt="image" class="w-100">
                </div>
            </div>
        </div>

        <div class="row home_info">

            <div class="col-md-4 p-0 d-flex justify-content-center">
                <div class="pl-4 pr-4 w-75 padd_bottom">
                    <div class="d-flex align-items-baseline">
                            <img src="/img/home/cooking.jpg">
                            <span class="pl-3 home_info_title">{{ __('homepage.fresh') }}</span>
                    </div>

                    <div class="m-auto">
                        <span class="home_info_text">{{ __('homepage.production') }}</span>
                    </div>
                </div>
            </div>


            <div class="col-md-4 p-0 d-flex justify-content-center">
                <div class="pl-4 pr-4 w-75 padd_bottom">
                    <div class="d-flex align-items-baseline">
                        <img src="/img/home/delivery.jpg">
                        <span class="pl-3 home_info_title">{{ __('homepage.delivery') }}</span>
                    </div>

                    <div class="m-auto">
                        <span class="home_info_text">{{ __('homepage.respond') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 p-0 d-flex justify-content-center">
                <div class="pl-4 pr-4 w-75 padd_bottom">
                    <div class="d-flex align-items-baseline">
                        <img src="/img/home/year.png">
                        <span class="pl-3 home_info_title">{{ __('homepage.experience') }}</span>
                    </div>

                    <div class="m-auto">
                        <span class="home_info_text">{{ __('homepage.confirmation') }}</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="row align-items-center" style="padding-top: 50px;">
            <div class="col-md-4 col-sm-6 pb-5">
                <div class="d-flex justify-content-center">
                    <img src="/img/home/pizza1.jpg" class="rounded-circle" style="width: 200px">
                </div>
            </div>

            <div class="col-md-8 col-sm-6 d-flex justify-content-center pb-5">
                <div class="w-75">
                    <div>
                        <h3 class="font-weight-bold">{{ __('homepage.pizza') }}</h3>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.recognizable') }}</span>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.pizza_prepare') }}</span>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.pizza_delivery') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row align-items-center" style="padding-top: 50px;">
            <div class="col-md-8 col-sm-8 d-flex justify-content-center pb-5">
                <div class="w-75">
                    <div>
                        <h3 class="font-weight-bold">{{ __('homepage.grill') }}</h3>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.grill_lovers') }}</span>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.grill_specialities') }}</span>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.grill_prepare') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 pb-5">
                <div class="d-flex justify-content-center">
                    <img src="/img/home/grill.jpg" class="rounded-circle" style="width: 200px">
                </div>
            </div>
        </div>

        <div class="row align-items-center" style="padding-top: 50px;">
            <div class="col-md-4 col-sm-4 pb-5">
                <div class="d-flex justify-content-center">
                    <img src="/img/home/sandwiches.jpg" class="rounded-circle" style="width: 200px">
                </div>
            </div>

            <div class="col-md-8 col-sm-8 d-flex justify-content-center pb-5">
                <div class="w-75">
                    <div>
                        <h3 class="font-weight-bold">{{ __('homepage.sandwiches') }}</h3>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.sandwiches_prepare') }}</span>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.hot_sandwiches') }}</span>
                    </div>

                    <div>
                        <span class="font_size_medium">{{ __('homepage.love_sandwiches') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

