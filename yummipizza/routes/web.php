<?php

use Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Session;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/email',function (){
    return new \App\Mail\ContactMail();
});

Route::get('/locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/', 'PagesController@index');
Route::get('/about-us','PagesController@about');

Route::get('/contact','ContactController@create');
Route::post('/contact','ContactController@store')->name('contact.store');

Route::get('/menu','MenuController@index');

Route::get('/menu/{category:slug}/{article:slug}','MenuController@show');

Route::get('/cart','CartController@index');
Route::post('/cart','CartController@store');
Route::patch('/cart/{cart}','CartController@update');
Route::delete('/cart/{cart}', 'CartController@destroy')->name('delete');

Route::get('/checkout','OrderController@index');
Route::post('/checkout', 'OrderController@store');
