<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    protected $fillable=[];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function translate($text){
        $locale=App::getLocale();
        $column=$text."_".$locale;
        return $this->$column;
    }

    public function portions(){
        return $this->hasMany(Portion::class);
    }

    public function article_price($id){
        $price=DB::table('articles')
            ->join('portions','articles.id','=','portions.article_id')
            ->select('portions.price','portions.title_en', 'portions.title_rs')
            ->where('portions.article_id','=',$id)
            ->get();

        return $price;
    }
}
