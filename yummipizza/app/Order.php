<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
        'article_name_en',
        'article_name_rs',
        'portion_en',
        'portion_rs',
        'quantity',
        'price',
        'sum',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
