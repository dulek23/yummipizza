<?php

namespace App\Http\Controllers;

use App\Article;
use App\Cart;
use App\Portion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{

    public function index(){
        $cart = Cart::all();
        $sum_price=Cart::sum('sum');
        //dd($cart);
        return view('cart.index',compact('cart'));
    }

    public function store(){

        $article=DB::table('articles')
            ->where('id','=',request('article_id'))
            ->first();

        $portion = DB::table('portions')
            ->where('price','=',request('price'))
            ->where('article_id','=',request('article_id'))
            ->first();


        $art = Article::findOrFail(request('article_id'));
        $port = Portion::findOrFail($portion->id);

        $art_name=DB::table('carts')
            ->where('article_name_en','=',$article->name_en)
            ->first();

        if(empty($art_name)){
            Cart::create([
                'article_name_en' => $article->name_en,
                'article_name_rs' => $article->name_rs,
                'portion_en' => $port->title_en,
                'portion_rs' => $port->title_rs,
                'quantity' => request('quantity'),
                'price' => request('price'),
                'sum' => request('quantity') * request('price')
            ]);
            return redirect()->back();
        }else{
            Session::flash('msg', __('cart.error'));
            return redirect()->back();
        }

    }

    public function update(Cart $cart){
        //dd($cart->price);

        $cart->update([
            'quantity' => request('quantity'),
            'sum' => request('quantity') * $cart->price
        ]);

        return redirect()->back();
    }

    public function destroy(Cart $cart){
        $cart->delete();
        return redirect()->back();
    }

    public function translateCart($text){
        $locale=App::getLocale();
        $column=$text."_".$locale;
        return $column;
    }

}
