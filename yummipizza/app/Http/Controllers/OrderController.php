<?php

namespace App\Http\Controllers;

use App\Cart;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function index(){
        $cart = Cart::all();
        $sum_price=Cart::sum('sum');

        return view('order.index',compact('cart','sum_price'));
    }

    public function store(){
        $data=request()->validate([
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ]);

        User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'address' => $data['address']
        ]);

        $cart=Cart::all();

        $user = User::all()->last();

        foreach ($cart as $c){
            $user->orders()->create([
                'article_name_en' => $c->article_name_en,
                'article_name_rs' => $c->article_name_rs,
                'portion_en' => $c->portion_en,
                'portion_rs' => $c->portion_en,
                'quantity' => $c->quantity,
                'price' => $c->price,
                'sum' => $c->sum
            ]);
        }

        Cart::truncate();

        Session::flash('success',__('cart.success'));

        return redirect('/cart');

    }
}
