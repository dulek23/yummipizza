<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function create(){
        return view('contact.create');
    }

    public function store(){
        $data=request()->validate([
            'name' => 'required',
            'email' => ['required','email'],
            'message' => 'required'
        ]);

        Mail::to('admin@yummipizza.com')->send(new ContactMail($data));

        Session::flash('success',__('contact.success'));

        return redirect()->back();
    }
}
