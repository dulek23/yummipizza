<?php

namespace App\Http\Controllers;

use App\Article;
use App\Cart;
use App\Category;
use App\Portion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class MenuController extends Controller
{
    public function index(){
        $cat=Category::all();

        return view('menu.index',compact('cat'));
    }

    public function show(Category $category,Article $article){
        $portion_price= DB::table('portions')
            ->where('article_id','=',$article->id)
            ->first();

        return view('menu.show',compact('category','article','portion_price'));
    }


}
