<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Category extends Model
{
    protected $fillable=[];

    public function articles(){
        return $this->hasMany(Article::class)->orderBy('name'.'_'.App::getLocale());
    }

    public function translateCategory($text){
        $locale=App::getLocale();
        $column=$text."_".$locale;
        return $this->$column;
    }

}
