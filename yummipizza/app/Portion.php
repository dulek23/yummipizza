<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Portion extends Model
{
    public function article(){
        return $this->belongsTo(Article::class);
    }

    public function translatePortions($text){
        $locale=App::getLocale();
        $column=$text."_".$locale;
        return $this->$column;
    }

    public function getPrice(){
        $pr=DB::table('portions')
            ->join('articles','id','=','article_id')
            ->select('portions.price')
            ->where('portions.'.$this->translatePortions('title'),'=',$title)
            ->get();

        return $pr;
    }
}
