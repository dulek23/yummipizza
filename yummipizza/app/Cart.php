<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Cart extends Model
{
    protected $fillable=[
        'article_name_en',
        'article_name_rs',
        'portion_en',
        'portion_rs',
        'quantity',
        'price',
        'sum'
    ];

    public function translateCarts($text){
        $locale=App::getLocale();
        $column=$text."_".$locale;
        return $this->$column;
    }
}
