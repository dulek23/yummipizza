#################################################################################
	Installation
#################################################################################


	1. The first you have to do is to install XAMPP(7.3.0), because the required version of php and apache should be 7.3.0. XAMPP should be installed on Local disk (C:) on your computer. In C:\xampp\htdocs create one folder named yummipizza.

	2. Download the project from the https://gitlab.com/dulek23/yummipizza in zip format, unzip it and all content from yummipizza-master folder copy to C:\xampp\htdocs\yummipizza.

	3. To add or edit anything environment variables in Windows, you need to go to System Properties first. Then, go to "Advanced system settings"
	   and under "Advanced" tab, click on "Environment Variables". For all users in "System variables" click on "Path" and then "Edit". Click on "New" and paste C:\xampp\php and then OK.
	
	4. All data are stored in the database. XAMPP is required to use the database. 
	   You need to run the mySQL server Fzin XAMPP. In URL of yours web browser paste http://localhost/phpmyadmin/ and click enter. Name of the database in phpmyadmin must have name yummi_pizza and then import database.

	5. In command prompt (CMD) write cd C:\xampp\htdocs\yummipizza\yummipizza and you access the folder where the project is installed. Write the command copy .env.example .env
	   In file .env change DB_DATABASE=laravel to DB_DATABASE=yummi_pizza

	6. In CMD write command cd C:\xampp\htdocs\yummipizza\yummipizza to access project folder and write command php composer.phar install
	
	7. After that write command php artisan key:generate

	8. Run the project with command php artisan serve

	9. Paste http://127.0.0.1:8000 in URL of your's web browser.

	
 