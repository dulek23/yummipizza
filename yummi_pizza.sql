-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2020 at 08:00 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yummi_pizza`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ingredients_en` text COLLATE utf8_unicode_ci NOT NULL,
  `ingredients_rs` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name_en`, `name_rs`, `ingredients_en`, `ingredients_rs`, `slug`, `image`, `category_id`) VALUES
(1, 'FRIED EGGS', 'JAJA NA OKO', 'Feta cheese, lettuce, fresh tomatoes, tortillas& peeled tomato, 4 x eggs', 'Feta sir, zelena salata, paradajz, tortilja, pelat', 'fried-eggs', 'breakfast/jaja_na_oko.png', 1),
(2, 'OMELET', 'OMLET', 'Olive oil, 4x eggs, sour cream, feta cheese, tomato, tortilla, peeled tomato, lettuce, rocket', 'Maslinovo ulje, 4xjaje, pavlaka, feta sir, paradajz, tortilja, pelat, zelena salata, rukola', 'omelet', 'breakfast/omlet.png', 1),
(3, 'BARON SANDWICH', 'BARON SENDVIČ', 'Toast, sour cream, cheese, ham, prosciutto, egg, lettuce, rocket, cherry', 'Tost, pavlaka, kačkavalj, praška šunka, njeguški pršut, jaje, zelena salata, rukola, čeri', 'baron-sandwich', 'breakfast/baron_sendvic.png', 1),
(4, 'FRIED BREAD IN EGGS', 'PRŽENICE', 'Baguette, fresh tomato, parmesan, feta cheese, sour cream spread, lettuce, rocket', 'Baget, svež paradajz, parmezan, feta sir, pavlaka namaz, zelena salata, rukola', 'fried-bread-in-eggs', 'breakfast/przenice.png', 1),
(5, 'GRILLED SAUSAGE AND EGGS', 'KOBAJA I JAJA', 'Eggs x 2, barbecue sausages x 3, tortilla with mustard & peeled tomato, feta cheese, green salad and tomato', 'Jaja na oko x 2, roštiljske kobasice x 3, tortilja sa senfom i pelatom, feta sir, zelena salata, paradajz', 'grilled-sausage-and-eggs', 'breakfast/jaja_i_kobaja.png', 1),
(6, 'BIANCO', 'BIANCO', 'Olive oil, garlic, olives, rocket, parmesan and tomato if desired', 'Maslinovo ulje, beli luk, masline, rukola, parmezan i paradajz po želji', 'bianco', 'pizza/bianco.png', 2),
(7, 'MARGARITA', 'MARGARITA', 'Tomato sauce, cheese, parmesan, tomatoes,  basil, olives', 'Pelat, sir, parmezan, svež paradajz, masline, bosiljak', 'margarita', 'pizza/margarita.png', 2),
(8, 'LIGHT', 'LIGHT', 'Olive oil, cheese, tomatoes, basil, rocket, olives', 'Maslinovo ulje, sir, paradajz, rukola, masline', 'light', 'pizza/light.png', 2),
(9, 'FUNGHI', 'FUNGHI', 'Tomato sauce, cheese, mushrooms , olives, dried boletus', 'Pelat, sir, šampinjoni, masline, sušeni vrganj', 'funghi', 'pizza/funghi.png', 2),
(10, 'VEGETARIANO', 'VEGETARIANO', 'Cheese, mushrooms, olives, tomatoes, rocket, pepper, red onion', 'Sir, šampinjoni, masline, paradajz, rukola, paprika, crveni luk', 'vegetariano', 'pizza/vegetariano.png', 2),
(11, 'CAPRICCIOSA', 'CAPRICCIOSA', 'Tomato sauce, cheese, ham, tomatoes, mushrooms, olives', 'Pelat, sir, praška šunka, svež paradajz, šampinjoni, masline', 'capricciosa', 'pizza/capricciosa.png', 2),
(12, 'VESUVIO', 'VESUVIO', 'Tomato sauce, cheese, ham, tomatoes, olives', 'Pelat, sir, praška šunka, paradajz, masline', 'vesuvio', 'pizza/vesuvio.png', 2),
(13, 'PARMA', 'PARMA', 'Tomato sauce, cheese, ham, mushrooms, parmesan, tomatoes, olives', 'Pelat, sir, praška šunka, šampinjoni, parmezan, paradajz, masline', 'parma', 'pizza/parma.png', 2),
(14, 'SERBIANA', 'SERBIANA ', 'Tomato sauce, cheese, bacon, mushrooms, sour cream, sausage, hot peppers', 'Pelat, sir, panceta, šampinjoni, pavlaka, kulen, ljute papričice', 'serbiana', 'pizza/serbiana.png', 2),
(15, 'QUATRO STAGIONE', 'QUATRO STAGIONE', 'Tomato sauce, cheese, ham, sausage, ham, sour cream, mushrooms, tomatoes, rocket, olives, egg', 'Pelat, sir, praška šunka, kulen, pršut, pavlaka, šampinjoni, paradajz, rukola, masline, jaje', 'quatro-stagione', 'pizza/quatro_stagione.png', 2),
(16, 'BOTAKO', 'BOTAKO', 'Tomato sauce, cheese, mushrooms, prosciutto, olives, parmesan, rocket', 'Pelat, sir, šampinjoni, njeguški pršut, dimljene masline, parmezan, rukola', 'botako', 'pizza/botako.png', 2),
(17, 'EL TORO', 'EL TORO', 'Tomato sauce, beef ham, mushrooms, olives, cheese, cheese, red onion', 'Pelat, goveđi pršut, šampinjoni, dimljene masline, sir, parmezan, crveni luk', 'el-toro', 'pizza/el_toro.png', 2),
(18, 'CALZONE', 'CALZONE', 'Tomato sauce, cheese, sausage, ham, cheese, smoked cheese', 'Pelat,sir, kulen, praška šunka, dimljeni sir, parmezan', 'calzone', 'pizza/calzone.png', 2),
(19, 'NLO', 'NLO', 'Olive oil, tomatoes, prosciutto, ham cheese, mushrooms, sour cream olives, parmesan', 'Maslinovo ulje, paradajz, njeguški pršut, praška šunka, sir, šampinjoni, pavlaka masline, parmezan', 'nlo', 'pizza/nlo.png', 2),
(20, 'MAIALE NERO', 'MAIALE NERO', 'Tomato sauce, mushrooms, seafood, lemon, olive oil, olives, garlic, rocket, tomatoes', 'Pizza sa kobasicom od mangulice, sir kačkavalj, čeri, brokoli, maslinovo ulje', 'maiale-nero', 'pizza/maiale_nero.png', 2),
(21, 'CAPRESE', 'CAPRESE', 'Tomato sauce, garlic, blanched tomatoes, basil,mozzarella, parmesan, olive oil', 'Pelat, beli luk, blanširani paradajz, bosiljak, mozzarella, peršun, parmezan', 'caprese', 'pasta/caprese.png', 3),
(22, 'QUATTRO FORMAGGI', 'QUATTRO FORMAGGI', 'Mozzarella, smoked cheese, gorgonzola, parmesan cheese, panna, walnut', 'Mozzarella, dimljeni sir, gorgonzola, parmezan, panna, orah', 'quattro-formaggi', 'pasta/quattro_formaggi.png', 3),
(23, 'FUNGHI AL VINO BIANCO', 'FUNGHI AL VINO BIANCO', 'Olive oil, porcini, mushrooms, garlic, chanterelles, truffles, wine Marsala, parmesan', 'Maslinovo ulje, vrganj, šampinjoni, tartuf, lisičarka, parmezan, beli luk, vino marsala', 'funghi-al-vino-bianco', 'pasta/funghi_al_vino_bianco.png', 3),
(24, 'CARBONARA', 'CARBONARA', 'Pancetta, ham, panna, egg yolk, parmesan', 'Pancetta, šunka, panna, žumance, parmezan', 'carbonara', 'pasta/carbonara.png', 3),
(25, 'ALL` AMATRICIANA', 'ALL` AMATRICIANA', 'Red onion, pancetta, garlic, tomatoes, parmesan, tomato sauce, pepperoncino, basil', 'Crveni luk, pančeta, beli luk, pelat, paradajz, parmezan, pepperoncino, bosiljak', 'all-amatriciana', 'pasta/all_amatriciana.png', 3),
(26, 'BOLOGNESE', 'BOLOGNESE', 'Beef, olive oil, garlic, olives, parmesan and tomatoes, parsley', 'Juneće meso, maslinovo ulje, beli luk, masline, parmezan i paradajz, peršun', 'bolognese', 'pasta/bolognese.png', 3),
(27, 'PROSCIUTTO', 'PROSCIUTTO', 'Olive oil, zucchini, prosciutto, panna, garlic', 'Maslinovo ulje, tikvice, njeguški pršut panna, beli luk, parmezan', 'prosciutto', 'pasta/prosciutto.png', 3),
(28, 'GAMBORI', 'GAMBORI', 'Tomato sauce, garlic, parmesan, white wine, olive oil, parsley', 'Pelat, crni luk, parmezan, limun, crveno vino, maslinovo ulje, peršun', 'gambori', 'pasta/gambori.png', 3),
(29, 'SMOKED SALOMON', 'DIMLJENI LOSOS', 'Olive oil, panna, zucchini, smoked olives, parsley', 'Maslinovo ulje, panna, tikvice, dimljene masline, peršun, parmezan', 'smoked-salomon', 'pasta/smoked_salomon.png', 3),
(30, 'FRUTTI DI MARE', 'FRUTTI DI MARE', 'Seafood, tomato sauce, fresh tomatoes, rocket, parsley, basil, parmesan', 'Morski plodovi, limun, pelat, svež paradajz, rukola, peršun, bosiljak, parmezan', 'frutti-di-mare', 'pasta/frutti_del_mare.png', 3),
(31, 'AGLIO E OLIO', 'AGLIO E OLIO', 'Olive oil, garlic, parmesan, pepperoncino, dried tomatoes', 'Maslinovo ulje, beli luk, parmezan, pepperoncino i sušeni paradajz', 'aglio-e-olio', 'pasta/aglio_e_olio.png', 3),
(32, 'FLAMBÉ STEAK', 'FLAMBIRANI STEK', 'Figs, dried tomatoes, parmesan, panna, peperoncino', 'Smokve, sušeni paradajz, parmezan, panna, pepperoncino, stock i crveno vino', 'flambe-steak', 'pasta/flambe_steak.png', 3),
(33, 'CHICKEN CURRY', 'PILETINA CURRY', 'Olive oil, panna, cherry, mushrooms, parmesan', 'Maslinovo ulje, čeri, panna, šampinjoni, grilovana piletina, kari, parmezan', 'chicken-curry', 'pasta/chicken_curry.png', 3),
(34, 'VEGETARIANO', 'VEGETARIANO', 'Sour cream, cheese, mushrooms, olives, tomatoes, peppers, onions', 'Pavlaka, sir, praška šunka, šampinjoni, masline, paradajz', 'vegetariano', 'pirogue/vegetariano.png', 4),
(35, 'QUATTRO FORMAGGI', 'QUATTRO FORMAGGI', 'Sour cream, smoked cheese, parmesan, gorgonzola, walnut, tomato', 'Pavlaka, gorgonzola, parmezan, dimljeni sir, svež paradajz, orah', 'quattro-formaggi', 'pirogue/quattro_formaggi.png', 4),
(36, 'CAPRICCIOSA', 'CAPRICCIOSA', 'Sour cream, cheese, ham, mushrooms, olives, tomato', 'Pavlaka, sir, praška šunka, šampinjoni, masline, paradajz', 'capricciosa', 'pirogue/capricciosa.png', 4),
(37, 'BOTAKO', 'BOTAKO', 'Sour cream, cheese, prosciuto, olives, mushrooms, tomatoes', 'Pavlaka, sir, njeguški pršut, masline, šampinjoni, paradajz', 'botako', 'pirogue/botako.png', 4),
(38, 'TOMATOES', 'PARADAJZ', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, svežim paradajzom, maslinama, začinjene mediteranskim biljem, pa zapečene u peći', 'tomatoes', 'bruschetti/tomatoes.png', 5),
(39, 'SMOKED SALMON', 'DIMLJENI LOSOS', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, svežim paradajzom, maslinama i dimljenim lososom, začinjene mediteranskim biljem, pa zapečene u peći', 'smoked-salomon', 'bruschetti/smoked_salomon.png', 5),
(40, 'TUNA', 'TUNA', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, svežim paradajzom, maslinama, i tunjevinom, začinjene mediteranskim biljem, pa zapečene u peći', 'tuna', 'bruschetti/tuna.png', 5),
(41, 'SOUR CREAM', 'PAVLAKA', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, svežim paradajzom, maslinama, pavlakom, sirom i parmezanom, začinjene mediteranskim biljem, pa zapečene u peći', 'sour-cream', 'bruschetti/sour_cream.png', 5),
(42, 'QUATTRO FORMAGGI', 'QUATTRO FORMAGGI', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, maslinama, pavlakom, parmezanom, dimljenim sirom, gorgonzolom i mozzarelom, orah, začinjene origanom, pa zapečene u peći', 'quattro-formaggi', 'bruschetti/quattro_formaggi.png', 5),
(43, 'HAM', 'ŠUNKA', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, svežim paradajzom, maslinama, pavlakom, šunkom, sirom i parmezanom, začinjene origanom, pa zapečene u peći', 'ham', 'bruschetti/ham.png', 5),
(44, 'BEEF HAM', 'GOVEĐI PRŠUT', 'Buns with garlic, fresh tomato, sour cream and cheese, olives baked in oven spiced with Mediterranean herbs', 'Ražane kajzerice sa belim lukom, svežim paradajzom, maslinama, pavlakom, goveđim pršutom, sirom i parmezanom začinjene mediteranskim biljem, pa zapečene u peći', 'beef-ham', 'bruschetti/beef_ham.png', 5),
(45, 'CAPRESE', 'CAPRESE', 'Tomatoes, mozzarella, fresh basil, rocket, olives, reduced balsamic', 'Paradajz, mozzarella, svež bosiljak, rukola, masline, redukovani aceto', 'caprese', 'salad/caprese.png', 6),
(46, 'SWEET AND SOUR', 'SLATKO KISELA', 'Mix lettuce, croutons, gorgonzola, cherry, raisins parmesan, sweet-sour sauce, reduced balsamic', 'Mix zelena salata, krutoni, čeri, suvo grožđe, gorgonzola, parmezan, slatko kiseli sos, redukovani aceto', 'sweet-and-sour', 'salad/sweet_and_sour.png', 6),
(47, 'ROCKET', 'RUKOLA ', 'Mix of green salad, rocket, cherry, parmesan, reduced aceto, croutons', 'Miks zelene salate, rukola, čeri, parmezan, redukovani aceto, krutoni', 'rocket', 'salad/rocket.png', 6),
(48, 'GRILLED GOAT CHEESE', 'SALATA GRILOVANI KOZJI SIR', 'Ruccola, lettuce, cherry, pear, croutons, walnut, dried grapes in cognac and honey, reduced aceto', 'Rukola, zelena salata, čeri, kruška, krutoni, orah, suvo grožđe u konjaku i medu, redukovani aceto', 'grilled-goat-cheese', 'salad/grilled_goat_cheese.png', 6),
(49, 'TUNA STEAK SALAD (FRESH TUNA)', 'SALATA TUNA STEK', 'Green salad, cherry, white and black sesame, chicory, teriyaki sauce, lime', 'Zelena salata, čeri, beli i crni susam, radič, Teriyaki sos, limeta, rukola', 'tuna-steak-salad', 'salad/tuna_steak_salad.png', 6),
(50, 'SMOKED SALMON SALAD', 'SALATA DIMLJENI LOSOS', 'Mix green salad, rocket, almond flakes, reduced aceto, apple, Roggenart croutons, sesame dressing, sliced mayonnaise', 'Mix zelenih salata, rukola, listići badema, redukovani aceto, jabuka, Roggenart krutoni, susam dresing, posni majonez', 'smoked_salmon_salad', 'salad/smoked_salmon_salad.png', 6),
(51, 'CAESAR (CHICKEN)', 'CEZAR (PILETINA)', 'Mix lettuce, cherry, parmesan, croutons, Caesar sauce, reduced balsamic', 'Rimska salata, čeri, parmezan, krutoni, Cezar sos, redukovani aceto', 'caesar', 'salad/caesar.png', 6),
(52, 'GREEN PITTED 150 G.', 'MASLINE ZELENE 150 G.', 'Green with stones', 'Zelene s košticom', 'green-pitted', 'olives/green_pitted.png', 7),
(53, 'OLIVES MIX 150 G.', 'MASLINE MIX 150 G.', 'Mix', 'Mix', 'mix', 'olives/mix.png', 7),
(54, 'OLIVES KALAMATA 150 G.', 'MASLINE KALAMATE 150 G.', 'Black Kalamat', 'Crne Kalamate', 'black_kalamata', 'olives/kalamata.png', 7),
(55, 'OLIVES BLACK DRIED 150 G', 'MASLINE CRNE SUŠENE 150 G', 'Black dried', 'Crne sušene', 'black-dried', 'olives/black_dried.png', 7),
(56, 'DUMPLINGS APRICOT', 'KNEDLE KAJSIJA', '(3 pieces)', '(3 kom. u porciji)', 'dumplings-apricot', 'dessert/dumpling_apricot.png', 8),
(57, 'DUMPLINGS PLUM', 'KNEDLE ŠJIVA', '(3 pieces)', '(3 kom. u porciji)', 'dumplings-plum', 'dessert/dumplings_plum.png', 8),
(58, 'DUMPLINGS CHERRY', 'KNEDLE VIŠNJA', '(3 pieces)', '(3 kom. u porciji)', 'dumplings-cherry', 'dessert/dumplings_cherry.png', 8),
(59, 'TRES LECHES', 'TRI LEĆE', 'Cake with three types of milk & caramel dressing', 'Kolač s tri vrste mleka i karamel prelivom', 'tres-leches', 'dessert/tres_leches.png', 8),
(60, 'PANNA COTTA RED FRUIT', 'PANAKOTA ŠUMSKO VOĆE', 'Red fruit+mint', 'Mix šumsko voće, nana', 'panna-cotta-red-fruit', 'dessert/panna_cotta_red_fruit.png', 8),
(61, 'BOTAKO CAKE RED FRUIT', 'BOTAKO CAKE ŠUMSKO VOĆE', 'Mix Red fruit', 'Mix crveno voće', 'botako-cake-red-fruit', 'dessert/botako_cake_red_fruit.png', 8),
(62, 'COCA COLA', 'COCA COLA', '0,33 l', '0,33 l', 'coca-cola', 'drink/coca_cola.png', 9),
(63, 'SPRITE', 'SPRITE', '0,33 l', '0,33 l', 'sprite', 'drink/sprite.png', 9),
(64, 'SCHWEPPES', 'SCHWEPPES', '0,33 l', '0,33 l', 'schweppes', 'drink/schweppes.png', 9),
(65, 'FANTA', 'FANTA', '0,33 l', '0,33 l', 'fanta', 'drink/fanta.png', 9),
(66, 'WATER', 'VODA', '0,5 l', '0,5 l', 'water', 'drink/water.png', 9),
(67, 'MINERAL WATER', 'KISELA VODA', '0,5 l', '0,5 l', 'mineral_water', 'drink/mineral_water.png', 9),
(68, 'YOGURT', 'JOGURT', '0,25 l', '0,25 l', 'yogurt', 'drink/yogurt.png', 9);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `article_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_name_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portion_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portion_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sum` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title_en`, `title_rs`, `slug`) VALUES
(1, 'Breakfast', 'Doručak', 'breakfast'),
(2, 'Pizza', 'Pica', 'piza'),
(3, 'Pasta', 'Pasta', 'pasta'),
(4, 'Pirogue', 'Piroške', 'pirogue'),
(5, 'Bruschetti', 'Brusketi', 'bruschetti'),
(6, 'Salad', 'Salate', 'salad'),
(7, 'Olives', 'Masline', 'olives'),
(8, 'Dessert', 'Poslastice', 'dessert'),
(9, 'Drinks', 'Piće', 'drinks');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_12_153348_create_categories_table', 1),
(5, '2020_08_12_153608_create_articles_table', 1),
(6, '2020_08_16_102128_create_portions_table', 1),
(7, '2020_09_09_103012_create_carts_table', 1),
(8, '2020_09_15_093649_create_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `article_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_name_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portion_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portion_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sum` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `article_name_en`, `article_name_rs`, `portion_en`, `portion_rs`, `quantity`, `price`, `sum`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'FRIED BREAD IN EGGS', 'PRŽENICE', 'Standard', 'Standard', '2', '350', '700', '1', '2020-09-18 17:59:41', '2020-09-18 17:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portions`
--

CREATE TABLE `portions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_rs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `portions`
--

INSERT INTO `portions` (`id`, `title_en`, `title_rs`, `price`, `article_id`) VALUES
(1, 'Standard', 'Standardna', '310', 1),
(2, 'Standard', 'Standardna', '310', 2),
(3, 'Standard', 'Standardna', '460', 3),
(4, 'Standard', 'Standardna', '350', 4),
(5, 'Standard', 'Standardna', '440', 5),
(6, 'Standard', 'Standardna', '530', 6),
(7, 'Family', 'Porodična', '790', 6),
(8, 'Standard', 'Standardna', '730', 7),
(9, 'Family', 'Porodična', '1080', 7),
(10, 'Standard', 'Standardna', '740', 8),
(11, 'Family', 'Porodična', '1090', 8),
(12, 'Standard', 'Standardna', '750', 9),
(13, 'Family', 'Porodična', '1090', 9),
(14, 'Standard', 'Standardna', '760', 10),
(15, 'Family', 'Porodična', '1140', 10),
(16, 'Standard', 'Standardna', '810', 11),
(17, 'Family', 'Porodična', '1190', 11),
(18, 'Standard', 'Standardna', '810', 12),
(19, 'Family', 'Porodična', '1190', 12),
(20, 'Standard', 'Standardna', '870', 13),
(21, 'Family', 'Porodična', '1330', 13),
(22, 'Standard', 'Standardna', '890', 14),
(23, 'Family', 'Porodična', '1350', 14),
(24, 'Standard', 'Standardna', '940', 15),
(25, 'Family', 'Porodična', '1440', 15),
(26, 'Standard', 'Standardna', '960', 16),
(27, 'Family', 'Porodična', '1440', 16),
(28, 'Standard', 'Standardna', '940', 17),
(29, 'Family', 'Porodična', '1420', 17),
(30, 'Standard', 'Standardna', '910', 18),
(31, 'Family', 'Porodična', '1360', 18),
(32, 'Standard', 'Standardna', '1380', 19),
(33, 'Family', 'Porodična', '2070', 19),
(34, 'Standard', 'Standardna', '890', 20),
(35, 'Family', 'Porodična', '1350', 20),
(36, 'Standard', 'Standardna', '730', 21),
(37, 'Standard', 'Standardna', '760', 22),
(38, 'Standard', 'Standardna', '840', 23),
(39, 'Standard', 'Standardna', '780', 24),
(40, 'Standard', 'Standardna', '790', 25),
(41, 'Standard', 'Standardna', '790', 26),
(42, 'Standard', 'Standardna', '840', 27),
(43, 'Standard', 'Standardna', '980', 28),
(44, 'Standard', 'Standardna', '950', 29),
(45, 'Standard', 'Standardna', '930', 30),
(46, 'Standard', 'Standardna', '670', 31),
(47, 'Standard', 'Standardna', '980', 32),
(48, 'Standard', 'Standardna', '790', 33),
(49, 'Standard', 'Standardna', '530', 34),
(50, 'Standard', 'Standardna', '620', 35),
(51, 'Standard', 'Standardna', '560', 36),
(52, 'Standard', 'Standardna', '620', 37),
(53, 'Standard', 'Standardna', '390', 38),
(54, 'Standard', 'Standardna', '540', 39),
(55, 'Standard', 'Standardna', '470', 40),
(56, 'Standard', 'Standardna', '410', 41),
(57, 'Standard', 'Standardna', '470', 42),
(58, 'Standard', 'Standardna', '430', 43),
(59, 'Standard', 'Standardna', '520', 44),
(60, 'Standard', 'Standardna', '620', 45),
(61, 'Standard', 'Standardna', '640', 46),
(62, 'Standard', 'Standardna', '700', 47),
(63, 'Standard', 'Standardna', '870', 48),
(64, 'Standard', 'Standardna', '930', 49),
(65, 'Standard', 'Standardna', '920', 50),
(66, 'Standard', 'Standardna', '790', 51),
(67, 'Standard', 'Standardna', '200', 52),
(68, 'Standard', 'Standardna', '250', 53),
(69, 'Standard', 'Standardna', '280', 54),
(70, 'Standard', 'Standardna', '260', 55),
(71, 'Standard', 'Standardna', '280', 56),
(72, 'Standard', 'Standardna', '280', 57),
(73, 'Standard', 'Standardna', '280', 58),
(74, 'Standard', 'Standardna', '290', 59),
(75, 'Standard', 'Standardna', '280', 60),
(76, 'Standard', 'Standardna', '250', 61),
(77, '0,33', '0,33', '80', 62),
(78, '0,33 l', '0,33 l', '90', 63),
(79, '0,33 l', '0,33 l', '90', 64),
(80, '0,33 l', '0,33 l', '90', 65),
(81, '0,33 l', '0,33 l', '70', 66),
(82, '0,33 l', '0,33 l', '70', 67),
(83, '0,33 l', '0,33 l', '70', 68);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Dusko Kovacevic', '0645333361', 'Goricani bb', '2020-09-18 17:59:41', '2020-09-18 17:59:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_category_id_index` (`category_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portions`
--
ALTER TABLE `portions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portions_article_id_index` (`article_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `portions`
--
ALTER TABLE `portions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `portions`
--
ALTER TABLE `portions`
  ADD CONSTRAINT `portions_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
